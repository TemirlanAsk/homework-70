import React, {Component} from 'react';
import {connect} from 'react-redux'
import {StyleSheet, Text, TouchableOpacity, View} from "react-native";

class Calculator extends Component{

    getTotalSum() {
        return (this.props.number)
    }

    render() {
        console.log(this.props.number);
        let  numbers = [];
        for (let i = 0; i < 10; i++) {
            numbers.push(<TouchableOpacity onPress={() => this.props.addNumber(i)}>
                <View style={styles.numbers}>
                    <Text>{i}</Text>
                </View>
            </TouchableOpacity>);

        }

        return(
            <View style={styles.container}>
                <Text>{this.getTotalSum()}</Text>
                {numbers}
                <View style={styles.symbols}>
                <TouchableOpacity onPress={() => this.props.sumNumber()} style={styles.symbols}>
                    <Text >+</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.subNumber()} style={styles.symbols}>
                    <Text>-</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.multiNumber()} style={styles.symbols}>
                    <Text>*</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.divisionNumber()} style={styles.symbols}>
                    <Text>/</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.pointNumber()} style={styles.symbols} >
                    <Text>.</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.rightNumber()} style={styles.symbols} >
                    <Text>(</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.leftNumber()} style={styles.symbols}>
                    <Text>)</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.percentNumber()}style={styles.symbols} >
                    <Text>%</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.cancelNumber()} style={styles.symbols}>
                    <Text>C</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.calculateNumber()} style={styles.symbols}>
                    <Text>=</Text>
                </TouchableOpacity>
            </View>
            </View>

        )
    }

}
const mapStateToProps = state => {
    return {
        number: state.nums
    }
};

const mapDispatchToProps = dispatch => {
    return{
        addNumber: number => dispatch({type: 'ADD_NUMBER', number}),
        sumNumber: sum => dispatch({type: 'SUM', sum}),
        subNumber: sub => dispatch({type: 'SUBTRACTION', sub}),
        multiNumber: multi => dispatch({type: 'MULTIPLICATION', multi}),
        divisionNumber: division => dispatch({type: 'DIVISION', division}),
        pointNumber: point => dispatch({type: 'POINT', point}),
        leftNumber: left => dispatch({type: 'LEFT', left}),
        rightNumber: right => dispatch({type: 'RIGHT', right}),
        percentNumber: percent => dispatch({type: 'PERCENT', percent}),
        cancelNumber: cancel => dispatch({type: 'CANCEL', cancel}),
        calculateNumber: calculate => dispatch({type: 'CALCULATE', calculate})

    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Calculator);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',


    },
    numbers: {
        width: 30,
        alignItems: 'center',
        borderWidth: 2,
        backgroundColor: 'grey'
    },
    symbols: {
        width: 30,
        alignItems: 'center',
        borderWidth: 2,
        borderColor: 'grey',
        backgroundColor: 'orange'
    }

});