const initialState = {
    nums: ''
};

const reducer  = (state = initialState, action) => {

    switch(action.type) {
        case 'ADD_NUMBER':
            return {
                ...state,
                nums: state.nums += action.number

            };
        case "SUM":
            return {...state, nums: state.nums + '+'};

        case "SUBTRACTION":
            return{...state, nums: state.nums + '-' };

        case "MULTIPLICATION":
            return{...state, nums: state.nums + '*'};

        case "DIVISION":
            return{...state, nums: state.nums + '/'};

        case "POINT":
            return{...state, nums: state.nums + '.'};

        case "LEFT":
            return{...state, nums: state.nums + ')'};

        case "RIGHT":
            return{...state, nums: state.nums + '('};

        case "PERCENT":
            return{...state, nums: state.nums + '%'};

        case "CANCEL":
            return{...state, nums: state.nums = ''};

        case 'CALCULATE':
            const result = eval(state.nums);
            return {...state, nums: result};

        default:
            return state
    }
};
export default reducer