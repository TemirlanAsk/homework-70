import React from 'react';
import Provider from "react-redux/es/components/Provider";
import {createStore} from "redux";
import reducer from "./src/store/reducer";
import Calculator from "./src/components/Calculator/Calculator";

const store = createStore(reducer);


export default class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <Calculator/>
            </Provider>
        );
    }
}


